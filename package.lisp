;;;; package.lisp

(in-package :cl-user)

(defpackage :tuple-trace
  (:use :cl :iterate :cl-tuples))

(in-package :tuple-trace)

