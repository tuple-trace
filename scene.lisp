
(in-package :tuple-trace)

(defclass scene ()
  ((primitives :accessor primitives-of 
               :initform (make-array 0 
                                     :element-type 'primitive 
                                     :adjustable t 
                                     :fill-pointer 0))))

(defgeneric primitive-of (scene index)
  (:method ((scene scene) index)
    (aref (primitives-of scene) index)))

(defgeneric add-primitive (scene primitive)
  (:method  ((scene scene) primitive)
    (vector-push-extend primitive (primitives-of scene))))

(defmethod (setf primitive-of) (primitive (scene scene) index)
  (setf (aref (primitives-of scene) index) primitive))

(defun make-scene (&rest primitives)
  (let 
      ((result (make-instance 'scene)))
    (loop
       for primitive in primitives
       do
         (add-primitive result primitive))
    result))
