
(in-package :tuple-trace)

(def-tuple-class material 
    (:tuples 
     ((colour :type colour))
     :slots
     ((reflection :initform 0.0 :accessor reflectivity-of :type single-float)
      (diffuse :initform 0.0 :accessor diffusion-of :type single-float))))

