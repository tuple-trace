
(in-package :tuple-trace)

(defclass image ()
  ((pixels :accessor pixels-of)
   (width :reader width-of)
   (height :reader height-of)))

(defmethod initialize-instance :after ((self image) &key width height)
  (setf (pixels-of self) (cl-tuples::make-colour-array (* width height)))
  (setf (slot-value self 'height) height)
  (setf (slot-value self 'width) width))

(defmethod pixel-of ((self image) x y)
  (make-colour
   (colour-aref (pixels-of self) (+ x (* y (width-of self))))))

(defmethod (setf pixel-of) (pixel (self image) x y)
  (setf (colour-aref  (pixels-of self) (+ x (* y (width-of self)))) (colour pixel)))


(defun make-image (w h)
  (let ((result (make-instance 'image :width w :height h)))
    result))


(defmethod dump-image ((self image) (filename pathname))
  (labels ((to-ubyte (x) 
              (round  (* 255 x))))
    (let* ((png 
            (make-instance 'zpng::png 
                           :width (width-of self)
                           :height (height-of self)))
           (image (zpng::data-array png)))
      (loop
         for i from 0 below (colour-array-dimensions (pixels-of self))
         do
           (with-colour-aref ((pixels-of self) i (r g b a))
             (setf (row-major-aref image (* 3 i)) (to-ubyte r))
             (setf (row-major-aref image (1+ (* 3 i))) (to-ubyte g))
             (setf (row-major-aref image (+ 2 (* 3 i))) (to-ubyte b))))
      (zpng::write-png png filename))))

