;;;; tuple-trace.asd
(in-package :asdf)

(asdf:defsystem :tuple-trace
  :depends-on (:cl-tuples :iterate :zpng)
  :serial t
  :components ((:file "package")
               (:file "ray")
               (:file "material")
               (:file "primitive")
               (:file "scene")
               (:file "image")
               (:file "camera")
               (:file "engine")))

