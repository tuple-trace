
(asdf:oos 'asdf:load-op 'tuple-trace)

(in-package :tuple-trace)


;; (defun new-primitive (scene &key init name reflectivity diffusion colour)
;;   (let ((result
;;          (destructuring-bind 
;;                (prim-type &key normal displacement centre radius)
;;              init
;;            (ccase prim-type  
;;              (:plane 
;;               (make-instance 'plane :name name 
;;                              :normal normal 
;;                              :displacement displacement))
;;              (:light (make-instance 'light :name name
;;                                     :centre centre
;;                                     :radius radius))
;;              (:sphere
;;               (make-instance 'sphere :name name
;;                              :centre centre
;;                              :radius radius))))))
;;     (when reflectivity
;;       (setf (reflectivity-of (material-of result)) reflectivity))
;;     (when diffusion 
;;       (setf (diffusion-of (material-of result)) diffusion))
;;     (when colour
;;       (setf (colour-of (material-of result)) colour))
;;     (add-primitive scene result)))

;; (defun test ()
;;   (let* ((raytracer  (make-instance 'raytracer))
;;          (scene (make-instance 'scene)))
;;     (new-primitive 
;;      scene
;;      (:plane :normal (make-vector3d* 0.0 1.0 0.0) :displacement 4.4)
;;      :name "Plane"                  
;;      :reflectivity 0.0 
;;      :diffusion 1.0 
;;      :color (make-colour* 0.4 0.3 0.3 1.0))
;;     (new-primitive 
;;      scene
;;      (:sphere 
;;       :centre (make-vertex3d*  1.0 -0.8 3.0 1.0)
;;       :radius 2.5) 
;;      :name  "Big Sphere"
;;      :reflectivity 0.6
;;      :colour (make-colour* 0.7 0.7 0.7))
;;     (new-primitive 
;;      scene
;;      (:sphere
;;       :centre (make-vertex3d* -5.5 -0.5 7.0 1.0)
;;       :radius 2.0)      
;;      :name "Small Sphere"
;;      :reflectivity 1.0
;;      :diffusion 0.1 
;;      :colour (make-colour* 0.7 0.7 1.0 1.0))
;;     (new-primitive
;;      scene
;;      (:light 
;;       :position (make-vertex3d*  0.0 5.0 5.0 1.0)
;;       :radius 0.1)
;;       :colour (make-colour* 0.6 0.6 0.6 1.0))     
;;     (new-primitive
;;      scene
;;      (:light
;;       :position (make-vertex3d* 2.0 5.0 1.0 1.0)
;;       :radius 0.1)
;;      :colour (make-colour* 0.7 0.7 0.9 1.0))
;;     (setf (scene-of raytracer) scene)
;;     (let ((result (render raytracer)))
;;       (write-png  result (merge-pathnames #P"raytraced.png")))))

;; create and plot a solid image

;; test ray - sphere intersection

(defun test-image ()
  (let ((image (make-image 320 200)))
    (loop
       for y from 0 to 50
       do
       (loop 
          for x from 0 to 320
          do
            (progn
              (setf (pixel-of image x y) (make-colour* 1.0 0.0 1.0 0.0))
              (setf (pixel-of image x (+ 50 y)) (make-colour*  0.0 0.0 1.0 0.0))
              (setf (pixel-of image x (+ 100 y)) (make-colour* 1.0 1.0 1.0 0.0)))))
    (dump-image image #P"/home/johnc/projects/tuple-trace/out.png")))

(defun test-scene ()
  (let ((scene (make-scene (make-sphere "sphere001"  0.0 0.0 10.0 5.0)))
        (ray (make-ray 0.0 0.0 -10.0 -0.7031 0.5156 10.0)))
    (loop 
         for primitive across (primitives-of scene)
         do (format t "Primitive ~A~%" primitive))
    (format t "Ray origin ~A Direction ~A~%" (origin-of ray) (direction-of  ray))
    (let ((interval
           (intersect (aref (primitives-of scene) 0) ray)))
      (format t "Intersection point ~A "
              (multiple-value-list
               (point-on ray interval))))))

(defun test-trace ()
  (let 
      ((camera (make-camera 0.0 0.0 -10.0))
       (scene (make-scene (make-sphere "sphere001" 0.0 0.0 10.0 5.0)))
       (image (make-image 320 200)))
    (render camera scene image)))