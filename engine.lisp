(in-package :tuple-trace)

(defconstant +trace-depth+ 6)


;; (defclass raytracer ()
;;   ((scene :accessor scene-of)
;;    (screen :accessor screen-of :initform (list -4.0 4.0 3.0 -3.0))))

;; (def-tuple-op add-diffuse-colour
;;     ((source-colour colour (rs gs bs as))
;;      (diffusion)
;;      (diffuse-color colour (rd gd bd ad))
;;      (light-colour colour (rl gl bl al)))
;;   (colour-tuple (+ (* diffusion rd rl) rs)
;;                 (+ (* diffusion gd gl) gs)
;;                 (+ (* diffusion bd bl) bs)
;;                 as))

;; (defun compute-intersection-colour (scene primitive intersection)
;;   (let ((intersection-colour (new-colour)))
;;     (loop
;;        for light across (primitives-of scene)
;;        when (typep primitive 'light)
;;        do (let* ((light-vector
;;                   (make-vector3d
;;                    (vector3d-normal
;;                     (delta-vector3d (vertex3d intersection)
;;                                     (vertex3d (centre-of light))))))
;;                  (normal (normal primitive intersection))
;;                  (material-colour (colour-of  (material-of primitive)))
;;                  (light-colour (colour-of (material-of light)))
;;                  (dot (vector3d-dot
;;                        (vector3d light-vector)
;;                        (vector3d normal))))
;;             (when (> dot 0)
;;               (setf (colour intersection-colour)
;;                     (add-diffuse-colour
;;                      (colour intersection-colour)
;;                      (diffusion-of (material-of primitive))
;;                      (colour material-colour)
;;                      (colour light-colour))))))
;;     intersection-colour))

;; (defmethod raytrace ((ray ray) (tracer raytracer) &optional (depth 0))
;;   (unless (> depth +trace-depth+)
;;     (let* ((scene (scene-of tracer))
;;            (primitives (primitives-of scene))
;;            (distance 11000000.0)
;;            (intersection-colour (new-colour)))
;;       (loop
;;          for primitive across primitives
;;          do
;;          (let ((intersection (intersect primitive ray distance)))
;;            (when intersection
;;              (if (typep primitive 'light)
;;                  (make-colour (colour-tuple  1.0 1.0 1.0 0.0))
;;                  (progn
;;                    (setf intersection-colour
;;                          (compute-intersection-colour scene primitive intersection)))))))
;;       intersection-colour)))

;; (defmethod render ((raytracer raytracer))
;;   (let* ((png (make-instance 'png :width 320 :height 200))
;;          (image (data-array png))
;;          (delta-x (/ (-  (third (screen-of raytracer)) (first (screen-of raytracer))) (width png)))
;;          (delta-y (/ (-  (fourth (screen-of raytracer)) (second (screen-of raytracer))) (height png))))
;;     (loop
;;        for current-y = (second (screen-of raytracer)) then (+ current-y delta-y)
;;        for target-y from 0 below (height png)
;;        do
;;        (loop
;;           for target-x from 0 below (width png)
;;           for current-x = (first (screen-of raytracer)) then (+ current-x delta-x)
;;           do
;;           (let ((ray (make-instance 'ray)))
;;             (progn
;;               (setf (direction-of ray)
;;                     (make-vector3d  (vector3d-normal (vector3d-tuple current-x current-y -5.0))))
;;               (setf (origin-of ray)
;;                     (make-vector3d (vector3d-tuple 0.0 0.0 -5.0)))
;;               (with-colour
;;                   (colour (raytrace ray raytracer))
;;                   (red green blue alpha)
;;                 (setf (aref image target-y target-x 0) (round  (* 255 red)))
;;                 (setf (aref image target-y target-x 1) (round  (* 255 green)))
;;                 (setf (aref image target-y target-x 2) (round  (* 255 blue))))))))
;;     png))


;; to do -- far/near clipping?

(defun render (camera scene image)
  (let ((pixel-dx (/ (window-width-of camera)
                     (width-of image)))
        (pixel-dy (/ (window-height-of camera)
                     (height-of image)))
        (ray (make-instance 'ray))
        (sphere-colour (make-colour* 1.0 0.0 0.0 0.0))
        (background-colour (make-colour* 1.0 1.0 1.0 0.0)))
    (loop
       for pixel-x from 0 below (width-of image)
       do
         (progn
           (format t "Tracing row ~A~%" pixel-x)
           (loop
              for pixel-y from 0 below (height-of image)
              do
              (let*
                  ((xp (+ (* 0.5 pixel-dx) (* pixel-x pixel-dx) (window-left-of camera)))
                   (yp (+ (* 0.5 pixel-dy) (* pixel-x pixel-dy) (window-top-of camera)))
                   (zp 0.0))
;;                (format t "XP ~A YP ~A~%" xp yp)
                (setf (origin-of ray) (position-of camera))
                (setf (direction-of ray) (vector3d-difference
                                          (vector3d*
                                           xp yp zp)
                                          (position-of camera)))
                (loop
                   for primitive across (primitives-of scene)
                   do
                     (let ((intersection (intersect primitive ray)))
                        (when intersection
                          (setf (pixel-of image pixel-x pixel-y) sphere-colour)
                          (format nil "Intersections @ ~A " intersection))
                        (setf (pixel-of image pixel-x pixel-y) background-colour)))))))))
