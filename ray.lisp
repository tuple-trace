

(in-package :tuple-trace)

;; this is a small enough thing it might be a primitive in cl-tuples..
(def-tuple-class ray
    (:tuples
     ((origin :type vector3d)
      (direction :type vector3d))))


(defun make-ray (xo yo zo xr yr zr)
  (let ((result 
         (make-instance 'ray
                        :origin (make-vector3d* xo yo zo)
                        :direction (make-vector3d* xr yr zr))))
    result))

(defmethod distance-of ((self ray))
  (vector3d-length (direction-of self)))

(defmethod direction-normal-of ((self ray))
  (vector3d-normal (direction-of self)))

(def-tuple-op extrapolate
    ((origin vector3d (xo yo zo))
     (direction vector3d (xd yd zd))
     (alpha single-float))
  (:return vector3d
           (vector3d*
            (+ xo (* xd alpha))
            (+ yo (* yd alpha))
            (+ zo (* zd alpha)))))

(defmethod point-on ((self ray) interval)
  (extrapolate 
   (origin-of self)
   (direction-of self)
   interval))

;; test code
;; (defparameter *ray* (make-ray 0.0 1.0 0.0 0.0 -1.0 0.0))
;; (direction-of *ray*)
;; (distance-of *ray*)
;; (direction-normal-of  *ray*)
;; (setf (direction-of *ray*) #{ 0.0 -2.0 0.0 })
;; (direction-normal-of *ray*)
;; (point-on *ray* 4.0)