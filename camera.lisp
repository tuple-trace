(in-package :tuple-trace)

(def-tuple-class camera 
    (:tuples 
     ((position :type vector3d)
      (window :type rect))))

(defmethod window-left-of ((self camera))
  (aref (window-of% self) 0))

(defmethod window-right-of ((self camera))
  (aref (window-of% self) 1))

(defmethod window-top-of ((self camera))
  (aref (window-of% self) 2))

(defmethod window-bottom-of ((self camera))
  (aref (window-of% self) 3))

(defmethod window-width-of ((self camera))
  (width  (window-of self)))

(defmethod window-height-of ((self camera))
  (height (window-of self)))


(defun make-camera (x y z)
  (let ((result (make-instance 'camera :position (make-vector3d* x y z))))
          result))
