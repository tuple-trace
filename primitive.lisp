
(in-package :tuple-trace)

(defclass primitive ()
  ((name :accessor name-of :initarg :name)
   (material :accessor  material-of :initform (make-instance 'material))
   (shape :accessor shape-of :initarg :shape)
   (geometry :accessor geometry-of :initarg :geometry)))

(def-tuple-class sphere 
  (:tuples 
   ((centre :type vertex3d))
   :slots
   ((radius :initform 1.0 :initarg :radius :type single-float :accessor radius-of))))

;; ;; sphere --
(def-tuple-op vector-square
    ((v vector3d (x y z)))
  (:return single-float
           (+ (* x x) (* y y) (* z z))))


(def-tuple-op sphere-intersect    
     ((centre vertex3d (cx cy cz cw))
      (radius single-float)
      (ray-origin vector3d (xro yro zro))
      (ray-direction vector3d (xr yr zr)))
  "Compute the intersection of a sphere and a ray." 
  (:return (or  single-float null)
           ;; we could speed this up by assuming a == 1 & direction is unit
           ;; (format t "Ray-direction ~A ~A ~A " xr yr zr)
           ;; (format t "origin -> centre ~A  " (multiple-value-list (vector3d-difference ray-origin centre)))
           (let* ((a (vector3d-dot ray-direction ray-direction))
                  (b (* 2.0 (vector3d-dot  ray-direction  (vector3d-difference ray-origin centre))))
                  (c (- (vector3d-dot (vector3d-difference ray-origin centre) (vector3d-difference ray-origin centre)) (* radius radius)))
                  (d (- (* b b) (* 4.0 a c))))
             ;;  (format t "A ~A  B ~A C ~A D ~A ~%" a b c d)
             (when (> d 0)
               (/ (- (- b) (sqrt d)) (* 2.0 a))))))


(defun make-sphere (name x y z r)
  (let* ((sphere (make-instance 'sphere 
                               :centre (make-vertex3d* x y z 1.0)
                               :radius r))
        (result (make-instance 'primitive
                               :name name
                               :shape :sphere
                               :geometry sphere)))
    result))


(def-tuple-op sphere-normal
     ((centre vertex3d (cx cy cz cw))
      (point vertex3d (px py pz pw)))
  (:return vector3d
   (vector3d-normal (vector3d*  ( - cx px) (- cy py) (- cz pz)))))

(def-tuple-class plane 
  (:tuples
   ((normal :type vector3d))
   :slots 
   ((displacement :initform 0.0 :initarg :displacement :accessor displacement-of))))

(def-tuple-class light 
  (:tuples 
   ((centre :type vertex3d))
   :slots
   ((radius :initform 1.0 :initarg :radius :type single-float :accessor radius-of))))



(defun intersect (primitive ray)
  "Test primitive for intersection with ray and return array of
  intersection distances or NIL if no intersection."
  (let ((geometry (geometry-of primitive)))
    (ccase (shape-of primitive)
      (:sphere (sphere-intersect (centre-of geometry) (radius-of geometry) (origin-of ray) (direction-of ray))))))

(defun normal (primitive point)
   "Given a point on the surface of a primtive, return the normal"
   (let ((geometry (geometry-of primitive)))
     (ccase (shape-of primitive)
       (:sphere (sphere-normal (centre-of geometry) (vertex3d point))))))


;; (defmethod normal ((sphere sphere) point)
;;   (sphere-normal (vertex3d (centre-of sphere))
;;                  (vertex3d point)))
  
;; (defmethod intersect ((plane plane) point distance))

;; (defmethod normal ((plane plane) point))